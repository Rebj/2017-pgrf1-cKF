import rasterdata.RasterImage;
import rasterdata.RasterImageBuffered;
import rasterdata.RasterImageImmutable;
import rasterdata.RasterImagePresenterAWT;
import rasterops.LineRasterizer;
import rasterops.LineRasterizerNaive;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.util.function.Function;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

/**
 * trida pro kresleni na platno: zobrazeni pixelu
 *
 * @author PGRF FIM UHK
 * @version 2017
 */

public class Canvas {

	private final JFrame frame;
	private final JPanel panel;
	private final BufferedImage img;

	private RasterImage<Integer> rasterImage;
	private final LineRasterizer<Integer> liner;
	private int previousX, previousY;

	public Canvas(final int width, final int height) {
		frame = new JFrame();

		frame.setLayout(new BorderLayout());
		frame.setTitle("UHK FIM PGRF : " + this.getClass().getName());
		frame.setResizable(false);
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

		img = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		/*
		rasterImage = new RasterImageBuffered<>(img,
				// toInteger: Function<PixelType, Integer>
				Function.identity()
				,
				// toPixelType: Function<Integer, PixelType>
				Function.identity());
		*/
		rasterImage = new RasterImageImmutable<>(width, height, 0);

		liner = new LineRasterizerNaive<>();

		panel = new JPanel() {
			private static final long serialVersionUID = 1L;

			@Override
			public void paintComponent(Graphics g) {
				super.paintComponent(g);
				present(g);
			}
		};

		panel.setPreferredSize(new Dimension(width, height));

		panel.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				previousX = e.getX();
				previousY = e.getY();
			}
		});
		panel.addMouseMotionListener(new MouseAdapter() {
			@Override
			public void mouseDragged(MouseEvent e) {
				final double startX = previousX / (panel.getWidth() - 1.0); //k zamysleni: proc 1.0 a ne 1?
				final double startY = 1 - previousY / (panel.getHeight() - 1.0);
				final double endX = e.getX() / (panel.getWidth() - 1.0);
				final double endY = 1 - e.getY() / (panel.getHeight() - 1.0);
				clear(); // zkuste zakomentovat
				rasterImage = liner.rasterizeLine(rasterImage,
						startX, startY, endX, endY,
						0xffff00);
				panel.repaint();
			}
		});

		frame.add(panel, BorderLayout.CENTER);
		frame.pack();
		frame.setVisible(true);
	}

	public void clear() {
		rasterImage = rasterImage.cleared(0x2f2f2f);
	}

	public void present(final Graphics graphics) {
//		graphics.drawImage(img, 0, 0, null); //rychle, jen pro RasterImageBuffered
		new RasterImagePresenterAWT<>(rasterImage, Function.identity()).present(graphics); //pomale, obecne
	}

	public void draw() {
		clear();
		img.setRGB(10, 10, 0xffff00);
		rasterImage = liner.rasterizeLine(rasterImage,
				0.3, 0.3, 0.9, 0.6,
				0xff00ff);
	}

	public void start() {
		draw();
		panel.repaint();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Canvas(800, 600)::start);
	}

}